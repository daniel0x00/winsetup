# winsetup
Script for apply a first provisioning to Windows machine.

This PowerShell script will install all common applications needed for any IT professional in a Windows machine. It will install first [Scoop](http://scoop.sh/), which is a command-line package installer for Windows. Then, using Scoop, it will install this essential tools:
`7zip coreutils curl concfg pshazz grep openssh sed wget less nano netcat shasum touch unzip vim sysinternals firefox python ruby`

After that, it will natively install the following programs: `Google Chrome, Visual Studio Code, Vagrant and git`.

You can configure `winsetup.ps1` script to remove/add your custom programs.

---

##### Requirements

* [PowerShell 3](http://www.microsoft.com/en-us/download/details.aspx?id=34595). Only required for Windows 7 and older. Run `$PSVersionTable.PSVersion` to check your version.
* PowerShell Unrestricted Execution Policy. Open PowerShell console and type: `set-executionpolicy unrestricted -s cu`

##### Install
* Open a PowerShell console and run: `iex (new-object net.webclient).downloadstring('https://goo.gl/UL9Wcj')`

---

###### Console behaviour changes

This script will install [concfg](https://github.com/lukesampson/concfg) and install a custom theme. After install, you cmd and powershell console will look like this:
![alt text](http://ferreira.fm/winsetup/powershell.png "cmd and powershell console theme")
To come back to default cmd and powershell theme, run `concfg import defaults`

[pshazz](https://github.com/lukesampson/pshazz) will be installed too. It will improve you PowerShell console to soport Git, tab completions and more.

###### Git bash and aliases

This script will also add these [.gitconfig](https://github.com/daniel0x00/winsetup/blob/master/.gitconfig), [.minttyrc](https://github.com/daniel0x00/winsetup/blob/master/.minttyrc), [.bashrc](https://github.com/daniel0x00/winsetup/blob/master/.bashrc), [.bash_colors](https://github.com/daniel0x00/winsetup/blob/master/.bash_colors) and [.bash_aliases](https://github.com/daniel0x00/winsetup/blob/master/.bash_aliases) files. It will improve your git bash console to look like this:
![alt text](http://ferreira.fm/winsetup/gitbash.png "gitbash console theme")

Take a look at the bash aliases you will be available to use:
```bash
alias sl='ls'
alias cd..='cd ..'
alias gti='git'
alias gz='tar czvf'
alias ugz='tar xzvf'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
```

And these git aliases:
```bash
git st = status -sb
git ci = commit
git co = checkout
git br = branch
git df = diff
git pr = pull --rebase --stat
git lg = log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %Cblue<%an>%Creset' --abbrev-commit --date=relative --all
```


