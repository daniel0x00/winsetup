
## Windows first setup script.
## This script will install all common programs required for a IT security pentester to work.
## Feel free to adjust this script to your needs and also contribute to this project, I'll be glad to merge your pull request.
##
## Daniel Ferreira
## @daniel0x00

## Global editable vars:
## You can try to install a small list first, then add more and run winsetup.ps1 again.
$scoop_programs_to_install = '7zip coreutils curl concfg pshazz grep openssh sed wget less nano netcat shasum touch unzip vim firefox python ruby';

$native_programs_to_install = @"

[
    
    {
        "name": "Visual Studio Code",
        "url": "https://go.microsoft.com/fwlink/?LinkID=623230",
        "file": "vscode_installer.exe",
        "args": "/verysilent /mergetasks=!runcode"
    },

    {
        "name": "Google Chrome",
        "url": "http://dl.google.com/chrome/install/375.126/chrome_installer.exe",
        "file": "chrome_installer.exe",
        "args": "/silent /install"
    },
    
    {
        "name": "Git",
        "url": "https://github.com/git-for-windows/git/releases/download/v2.8.1.windows.1/Git-2.8.1-64-bit.exe",
        "file": "git_installer.exe",
        "args": "/verysilent /norestart /COMPONENTS='icons,ext\\reg\\shellhere,assoc,assoc_sh'"
    },
    
    {
        "name": "Vagrant",
        "url": "https://releases.hashicorp.com/vagrant/1.8.1/vagrant_1.8.1.msi",
        "file": "vagrant_installer.msi",
        "args": "/quiet /norestart"
    }
]

"@ | convertfrom-json; 

## URL vars:
$url_files_to_home_directory = @"

[
    
    {
        "url": "https://bitbucket.org/daniel0x00/winsetup/raw/0a2d7552745969d2ebdc5e60e17302db38862f34/.gitconfig",
        "file": ".gitconfig"
    },

    {
        "url": "https://bitbucket.org/daniel0x00/winsetup/raw/0a2d7552745969d2ebdc5e60e17302db38862f34/.bashrc",
        "file": ".bashrc"
    },
    
    {
        "url": "https://bitbucket.org/daniel0x00/winsetup/raw/0a2d7552745969d2ebdc5e60e17302db38862f34/.bash_colors",
        "file": ".bash_colors"
    },
    
    {
        "url": "https://bitbucket.org/daniel0x00/winsetup/raw/0a2d7552745969d2ebdc5e60e17302db38862f34/.bash_aliases",
        "file": ".bash_aliases"
    },
    
    {
        "url": "https://bitbucket.org/daniel0x00/winsetup/raw/0a2d7552745969d2ebdc5e60e17302db38862f34/.minttyrc",
        "file": ".minttyrc"
    }
]

"@ | convertfrom-json; 

$url_concfg_configuration_file = 'https://bitbucket.org/daniel0x00/winsetup/raw/0a2d7552745969d2ebdc5e60e17302db38862f34/concfg_flat.json';


##  Begin install
write-host @'
        .__                      __                
__  _  _|__| ____   ______ _____/  |_ __ ________  
\ \/ \/ /  |/    \ /  ___// __ \   __\  |  \____ \ 
 \     /|  |   |  \\___ \\  ___/|  | |  |  /  |_> >
  \/\_/ |__|___|  /____  >\___  >__| |____/|   __/ 
                \/     \/     \/           |__|    
                

Please run this script with the following setup:
* If your OS is 64 bits, run this script from a 64 bits PowerShell console (not the x86 version).
* Run this script in a elevated PowerShell console (right click, run as Administrator).
                
'@
    
$global_user_fullname = Read-Host -Prompt 'Input your full name';
$global_user_email = Read-Host -Prompt 'Input your email';
write-host ''
write-host ''

## Check if 'scoop' is installed looking in the %path% environment var. 
if ($env:path -notmatch 'scoop') { 
    ## installing scoop:
    write-host '##'
    write-host 'installing scoop...'
    write-host '##'

    iex (new-object net.webclient).downloadstring('https://get.scoop.sh');
    start-process scoop.cmd 'bucket add extras' -nonewwindow -wait;
}
## Check if scoop extras bucket is installed:
else {
    $scoop_extras_path = [io.path]::combine($env:appdata,'../Local/scoop/buckets/extras');
    if (-not (test-path $scoop_extras_path)) { start-process scoop.cmd 'bucket add extras' -nonewwindow -wait; }
}

## Install a set of scoop programs previously specified.
write-host '##'
write-host 'installing scoop programs...'
write-host '##'

## Iterate programs to install:
foreach ($program in $scoop_programs_to_install -split '\s+') { start-process scoop.cmd "install $program" -nonewwindow -wait; }

## Install native programs previously specified.
write-host '##'
write-host 'installing native programs...'
write-host '##'

## Create winsetup on %temp% folder:
new-item -itemtype directory -force -path $env:temp/winsetup | out-null

## Download and install programs one by one:
foreach ($program in $native_programs_to_install) {
    $program_file = [io.path]::combine($env:temp,'winsetup',$program.file);
    write-host 'downloading' $program.name '...';
    (New-Object System.Net.WebClient).DownloadFile($program.url, $program_file);
    write-host 'installing' $program.name '...';
    if ($program_file -match '\.msi$') { start-process -filepath 'msiexec.exe' -argumentlist "/i $program_file $($program.args)" -wait -nonewwindow; }
    else { start-process $program_file -ArgumentList $program.args -NoNewWindow -Wait; }
}

## Install home files:
write-host '##'
write-host 'installing home files...'
write-host '##'

## Download and install files one by one:
foreach ($homefile in $url_files_to_home_directory) {
    $homefile_file = [io.path]::combine($env:userprofile,$homefile.file);
    write-host 'downloading' $homefile.file '...';
    (New-Object System.Net.WebClient).DownloadFile($homefile.url, $homefile_file);
}

## Configuring programs:
write-host '##'
write-host 'configuring programs...'
write-host '##'

## Configuring git:
##write-host 'configuring git...'
##start-process git "config --global user.name `"$global_user_fullname`" --replace-all" -nonewwindow -wait;
##start-process git "config --global user.email `"$global_user_email`" --replace-all" -nonewwindow -wait;

## Configuring git:
write-host 'configuring concfg... please answer "Y" to "overrides in the registry..." question and then "n" for next question'
start-process concfg.cmd "import $url_concfg_configuration_file" -nonewwindow -wait;

## Finish print:
write-host ''
write-host ''

write-host '##'
write-host 'winsetup done. reboot computer recommended.'
write-host '##'
write-host ''